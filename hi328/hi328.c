/*
 * hi328.c
 *
 * Created: 15.11.2015 20:34:19
 *  Author: stek
 */ 
#define F_CPU 16000000L
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "stdio.h"
#include "string.h"
#include "stdlib.h"

#define FOSC F_CPU // Clock Speed
#define BAUD 9600
#define MYUBRR FOSC/16/BAUD-1
typedef struct{
	uint16_t us;
	uint8_t R;
	uint8_t G;
	uint8_t B;	
// 	uint16_t ms;
// 	uint8_t s;
// 	uint8_t m;
// 	uint8_t h;
// 	uint8_t d;	
}time_t;

volatile time_t currentTime = {0};

void USART_Init(unsigned int ubrr){
	UBRR0H = (unsigned char)(ubrr>>8);
	UBRR0L = (unsigned char)ubrr;
	/*Enable receiver and transmitter */
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	/* Set frame format: 8data, 2stop*/
	UCSR0C = (1<<USBS0)|(3<<UCSZ00);
}

void USART_Transmit( unsigned char data )
{
	while ( !( UCSR0A & (1<<UDRE0)) );
	UDR0 = data;
}


/*
void uprint( uint8_t size, char* str)
{
	for(uint8_t i = 0; i < size; i++)
	{
		UDR0 = str[i];
		while( !(UCSR0A & (1 << UDRE0)) ){};
	}
	UDR0 = '\r';
	while( !(UCSR0A & (1 << UDRE0)) ){};
	UDR0 = '\n';
	while( !(UCSR0A & (1 << UDRE0)) ){};
	return SUCCESS;
}
*/
char uart_printf_buf[128];
uint8_t delayUs(uint16_t delay);
uint8_t decVal(uint8_t R);
uint8_t incVal(uint8_t R);
void uprint(char *arg_list, ...)
{
	uint8_t i, str_size;
	va_list arg_buffer;
	va_start(arg_buffer, arg_list);
	memset(uart_printf_buf, 0, sizeof(uart_printf_buf));
	str_size = vsprintf(uart_printf_buf, arg_list, arg_buffer);
	for (i = 0; i < str_size; i++){
		USART_Transmit(uart_printf_buf[i]);
	}
}

void set_pwm(uint16_t val)
{
	TCCR1B|=(1<<WGM13)|(1<<WGM12)|(0<<CS11)|(0<<CS10); //�������� ������� = 64; ����� = 14(������� ���)
	OCR1A=val; 
	TCCR1B|=(1<<WGM13)|(1<<WGM12)|(1<<CS11)|(1<<CS10); //�������� ������� = 64; ����� = 14(������� ���)
}
void setSysTick(void)
{
	TCNT2 = 0;
	OCR2A = 200;
	TCCR2A = WGM21;
	TIMSK2 = _BV(OCIE2A);
	TIFR2 = OCF2A;
	TCCR2B = 1;
}
typedef enum{
	Green,
	Red,
	Blue	
}led_t;
void ledOn(led_t led)
{
	PORTB &= ~(1<<led);
	
}
void ledOff(led_t led)
{
	PORTB |= (1<<led);
}
uint8_t incVal(uint8_t R)
{
	if (R == 200){
		return 200;
	}else{
		R+=1;
		return R;
	}
}
uint8_t decVal(uint8_t R)
{
	if (R == 0){
		return 0;
	}else{
		R-=1;
		return R;
	}
}
void setColor(uint8_t R, uint8_t G, uint8_t B)
{
	currentTime.R = R;
	currentTime.G = G;
	currentTime.B = B;
	ledOn(Red);
	ledOn(Green);
	ledOn(Blue);
	while (currentTime.R||currentTime.G||currentTime.B)
	{
		if (!currentTime.R)
		{
			ledOff(Red);
		}
		if (!currentTime.G)
		{
			ledOff(Green);
		}
		if (!currentTime.B)
		{
			ledOff(Blue);
		}
	}	
}
// 	USART_Init(MYUBRR);
// 	uprint("hi\r\n");
typedef struct{
	uint8_t Red;
	uint8_t Green;
	uint8_t Blue;	
}color_t;
const color_t ColorTable[] = {{255,0,0},{255,128,0},{255,255,0},{0,255,0},{0,255,255},{0,0,255},{255,0,255},{255,0,0}};
static void SetColour(color_t color)
{
	currentTime.R = color.Red;
	currentTime.G = color.Green;
	currentTime.B = color.Blue;
	ledOn(Red);
	ledOn(Green);
	ledOn(Blue);
	while (currentTime.R||currentTime.G||currentTime.B)
	{
		if (!currentTime.R)
		{
			ledOff(Red);
		}
		if (!currentTime.G)
		{
			ledOff(Green);
		}
		if (!currentTime.B)
		{
			ledOff(Blue);
		}
	}
}
static uint8_t ChangeColor(color_t start, color_t end, uint8_t steps, uint16_t delay/*��������� ����������, ���� ������*/)
{
	SetColour(start);
	//����� �� ���������� ��� ����� �������� ����
	
	SetColour(end);
	return 0;
}
int main()
{
	uint8_t i,j = 0;
	uint8_t r,g,b = 0;

	setSysTick();

	DDRB |= (1<<PB1)|(1<<PB2)|(1<<PB0); 
	PORTB |= (1<<Red)|(1<<Blue)|(1<<Green);
		
	sei();
	while(1)
 	{
		 for (j = 0;j < 7;j++)
		 {
			 for (i = 200;i>0;i--)
			 {
				 SetColour(ColorTable[j]);
			 }
		 }
		 
		
	}
	return 0;
}
uint8_t delayUs(uint16_t delay)
{
	//1 = 10kHz*8 = 80kHz
	currentTime.us = delay;
	while (currentTime.us)
	{
	}
	return 0;
}
//systick timer +software timers ��� ������� ����

ISR(TIMER2_COMPA_vect)
{
	if(currentTime.us)
	{
		currentTime.us -= 1;	
	}
	if(currentTime.R)
	{
		currentTime.R -= 1;
	}
	if(currentTime.G)
	{
		currentTime.G -= 1;
	}
	if(currentTime.B)
	{
		currentTime.B -= 1;
	}
}